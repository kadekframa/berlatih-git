<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

// Code function di sini
function greetings($name) {
    echo "Hallo ".$name.", Selamat Datang di Sanbercode! <br>";
}

// Hapus komentar di bawah ini untuk jalankan code.
greetings("Kadek Frama");
greetings("Made");
greetings("Nyoman");

echo "<br>";


echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini.
function balikan($kata2) {
    $length_kata = strlen($kata2);
    $tampung_kata = null;

    for($i = $length_kata-1; $i>=0; $i--) {
        $tampung_kata .= $kata2[$i];
    }

    return $tampung_kata;
}

function reverseString($kata1) {
    $balikKatanya = balikan($kata1);
    echo $kata1 . " => " . $balikKatanya . "<br>";
}

// Hapus komentar di bawah ini untuk jalankan code.
reverseString("civic");
reverseString("cicak");
reverseString("isi");
reverseString("semangat");
reverseString("katak");
reverseString("racecar");
echo "<br>";


echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/

// Code function di sini.
function palindrome($cek_kata) {
    $balikString = balikan($cek_kata);
    echo ($balikString === $cek_kata) ? $cek_kata ." => true <br>" : $cek_kata ." => false <br>";
}

// Hapus komentar di bawah ini untuk jalankan code.
palindrome('civic');
palindrome('isi');
palindrome('semangat');
palindrome('racecar');


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($value) {
    if($value >= 85 && $value <= 100) {
        return $value . " => Sangat Baik <br>";
    } else if ($value >= 70 && $value <= 85) {
        return $value . " => Baik <br>";
    } else if ($value >= 60 && $value <= 70) {
        return $value . " => Cukup <br>";
    } else {
        return $value . " => Kurang <br>";
    }
}

// Hapus komentar di bawah ini untuk jalankan code.
echo tentukan_nilai(94);
echo tentukan_nilai(84);
echo tentukan_nilai(74);
echo tentukan_nilai(64);


?>

</body>

</html>